# JAIRO Crawler-List #

"**JAIRO Crawler-List**"は国立情報学研究所が提供する国内機関リポジトリのための共用クローラーリスト（ロボットリスト）です。

機関リポジトリをはじめWebアプリケーションの利用統計においては、検索エンジン（Google等）のクローラー（ロボット）によるアクセスを排除することで、より利用の実態に即した統計をとることができます。
しかし、アクセス記録の排除に使うクローラーのリストを維持することは大変です。また、個々の機関で別のクローラーリストを利用すると、機関リポジトリ間の利用状況の比較を正確に行うことができません。JAIRO Crawler-Listは、これらの問題について解決します。

##1.利用のメリット ##
### 1-(1) メンテナンス不要のクローラーリスト ###
　　クローラーリストのメンテナンスは国立情報学研究所で行います。更新頻度は年1回を予定しています。

###1-(2)機関リポジトリ同士の利用状況の比較 ###
　　JAIRO Crawler-Listを使った機関同士であれば、利用統計を同じ条件で比較できます。


##2.利用法 ##

JAIRO Crawler-Listは、「IPアドレスリスト」と「ユーザーエージェントリスト」の2ファイルに分かれています。
それぞれをダウンロードし、Webサーバーのアクセスログとマッチングさせ、マッチした場合は利用統計の対象から外すことで、クローラーからのアクセスを除外したアクセス集計を行えるようになります。
データ形式は後述のとおりです。IPアドレスは完全一致で、ユーザーエージェントは部分一致でマッチさせる必要があります。マッチングにおいて大文字と小文字は区別します。

##3.入手##
最新リストのダウンロードURL（固定）は次のとおりです。

* IPアドレスリスト
[ https://bitbucket.org/niijp/jairo-crawler-list/raw/master/JAIRO_Crawler-List_ip_blacklist.txt ]( https://bitbucket.org/niijp/jairo-crawler-list/raw/master/JAIRO_Crawler-List_ip_blacklist.txt)

* ユーザーエージェントリスト
[https://bitbucket.org/niijp/jairo-crawler-list/raw/master/JAIRO_Crawler-List_useragent.txt ](https://bitbucket.org/niijp/jairo-crawler-list/raw/master/JAIRO_Crawler-List_useragent.txt)

機関リポジトリの利用統計にJAIRO Crawler-Listをご利用の際は、「**JAIRO Crawler-ListのVersion *xxx*を使用した**」旨を記載くださいますようお願いをいたします。

##4.データ形式##

JAIRO Crawler-Listのデータ形式は、改行区切りのテキストファイルです。個々のクローラー情報が１行として記載されています。

###4-(1) コメント行###
行頭が#の行はコメント行です。

###4-(2)メタデータ記述###
コメント行に、メタデータを記述しています。形式は以下のとおりです。

* $ で挟まれた文字列がメタデータ記述部です。
* メタデータは、タグ名と値からなります。タグ名と値の区切りは「: 」です。
* タグは、**Version**, **Date**, **Author**の3つあります。

**Version**はバージョン番号を示すタグです。バージョン番号はメジャーバージョン（数値）とマイナーバージョンを「.」で繋いで示します。
COUNTERのエージェントリストが更新された場合はメジャーバージョンを、それ以外の更新はマイナーバージョンをアップします。
具体例は以下のとおりです。
```
# $Version: 1.0 $
```

**Date**は更新日付を示すタグです。日付形式は ISO-8601によります。
具体例は以下のとおりです。
```
# $Date: 2015-03-30 15:56:30 +0900 (Mon, 30 Mar 2015) $
```

**Author**はJAIRO Crawler-Listの著者を示すタグです。
具体例は以下のとおりです。
```
# $Author: National Institute of Informatics $
```

##5.メンテナンス ###
JAIRO Crawler-Listは、次の2つの情報をもとにメンテナンスを行っています。更新頻度は年1回の予定です。

###5-(1)JAIROのアクセスログ ###
国立情報学研究所がサービスするJAIRO ([http://ju.nii.ac.jp/](http://ju.nii.ac.jp/)では、国立情報学研究所が毎月クローラーからのアクセスをチェックしています。この情報をJAIRO Crawler-Listに反映しています。

###5-(2)COUNTERのユーザーエージェントリスト ###
電子ジャーナルの利用統計で利用が進んでいるCOUNTER（[http://www.projectcounter.org/](http://www.projectcounter.org/)） では、COUNTERとして排除すべきユーザーエージェントのリストを公開しています。このユーザーエージェントリストのユーザエージェントは正規表現で記載されてますが、JAIRO Crawler-Listでは部分一致で処理できるように若干の加工を行っています。

##6.謝辞##
JAIRO Crawler-Listは、ROAT  (Repositry Output Assessmennt Tool) における、クローラー（ロボット）リストの共有の考えをもとに、
[機関リポジトリ推進委員会](https://ir-suishin.repo.nii.ac.jp/)技術WGにおいて、クローラーリストの維持や活用等について検討を行い、具体化したものです。
ROAT関係者ならびに機関リポジトリ推進委員会技術WGに、この場を借りて感謝を申し上げます。

---

# JAIRO Crawler-List #
"JAIRO Crawler-List" is a web crawler (robot) list for statistics of institutional repositories in Japan. This list is provided by National Institute of Informatics (Japan).
You can take more actual statistics of institutional repository, If you remove web access log records that is accessed from search engine (Google, etc) by matching crawler (robot) list and access log. However, it is hard working for administrator of institutional repository to maintain the crawler list. It is difficult to comparison statistics of Institutional Repositories each other actually. JAIRO Crawler-List will solve these problems.

## 1. Benefit ##

### 1-(1) Maintenance is not necessary ###
National Institute of Informatics (Japan) maintenances the crawler list once a year.

### 1-(2) It makes possible to comparison statistics of institutional repositories each other. ###
If you use JAIRO Crawler-List, you can compare your statistics of Institutional Repository with the other one that use JAIRO Crawler-List in same conditions.

## 2. Usage ##
JAIRO Crawler-List includes two files. One file is a "IP address list", and another one is a "User agent list". Download these two files, and matching the record of web access log. To remove the web access log record that is matched the JAIRO Crawler-List record, You can get statistics that exclude access of web crawlers. Data format of JAIRO Crawler-List is as follows. IP address List is for exactly match, and User agent list is for partial match.　Lowercase and uppercase letters are distinguished in this match.

## 3. Download ##
Download URL (permalink) of the latest version is of JAIRO Crawler-List as follows.

* IP address list
[ https://bitbucket.org/niijp/jairo-crawler-list/raw/master/JAIRO_Crawler-List_ip_blacklist.txt ]( https://bitbucket.org/niijp/jairo-crawler-list/raw/master/JAIRO_Crawler-List_ip_blacklist.txt )

* User agent list
[https://bitbucket.org/niijp/jairo-crawler-list/raw/master/JAIRO_Crawler-List_useragent.txt ](https://bitbucket.org/niijp/jairo-crawler-list/raw/master/JAIRO_Crawler-List_useragent.txt)


If you use JAIRO Crawler-List, Please write "Powerd by JAIRO Crawler-List　version *xxx*" to your statistics.

##4.Data Format ##
Data format of JAIRO Crawler-List is text file separated by return code.

###4-(1) Comment Out ###
"#" is a comment out sign.

###4-(2) Metadata ###
Metadata of list is written in comment line. The format is as follows.
　•Caracters divided by "$" is metadata block.
　•Metadata block has tag name and value. The separation caracter of tag name and value is ":".
　•There are three tags, Version, Date, Author.
Version means the version number of JAIRO Crawler-List. Major version and minor version is connected by ".". If COUNTER agent list is updated, we will increment major version no. At other case, we increment minor version number. For example.
```
# $Version: 1.0 $
```

Date means update date。Date format is ISO-8601. For example.
```
# $Date: 2015-03-30 15:56:30 +0900 (Mon, 30 Mar 2015) $
```

Author means a creator of JAIRO Crawler-List. For example.
```
# $Author: National Institute of Informatics $
```

##5. Maintenance ##
JAIRO Crawler-List is maintenance by two information sources.　Update frequency of the list is per year.

### 5-(1) Access log of JAIRO ###
National Institute of Informatics check crawler access of JAIRO (http://ju.nii.ac.jp/) once a month. We apply it to JAIRO Crawler-List.

### 5-(2) User agent list of COUNTER ###
COUNTER（http://www.projectcounter.org/） provides user agent list. We processing the list to use partial match.

## 6.Acknowledgement ##
JAIRO Crawler-List is based on ROAT (Repository Output Assessment Tool). Institutional Repositories Promotion Committee (Japan) Technical WG investigation it.